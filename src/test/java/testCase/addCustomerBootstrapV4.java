package testCase;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import static org.junit.Assert.*;
import java.util.concurrent.TimeUnit;

public class addCustomerBootstrapV4 {

    @Test
    public void addNewCustomer() {
        // Parametros inicias para ingressar no site e maximizar o mesmo
        System.setProperty("webdriver.chrome.driver", "C:\\chromeDriver\\chromedriver.exe");
        WebDriver browser = new ChromeDriver();
        browser.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        browser.manage().window().maximize();

        // Abertura do link reportado + alteração do tema do bootstrap
        browser.get("https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");
        WebElement changeTheme = browser.findElement(By.id("switch-version-select"));
        changeTheme.click();
        changeTheme.sendKeys("Bootstrap V4 Theme");
        changeTheme.click();

        // Click no botão para adicionar registros + massa de dados inseridos
        WebElement btnAddRecord = browser
                .findElement(By.cssSelector("a[href='/v1.x/demo/my_boss_is_in_a_hurry/bootstrap-v4/add']"));
        btnAddRecord.click();
        browser.findElement(By.id("field-customerName")).sendKeys("Teste Sicredi");
        browser.findElement(By.id("field-contactLastName")).sendKeys("Teste");
        browser.findElement(By.id("field-contactFirstName")).sendKeys("Bruno Antonio de Oliveira");
        browser.findElement(By.id("field-phone")).sendKeys("51 9999-9999");
        browser.findElement(By.id("field-addressLine1")).sendKeys("Av Assis Brasil, 3970");
        browser.findElement(By.id("field-addressLine2")).sendKeys("Torre D");
        browser.findElement(By.id("field-city")).sendKeys("Porto Alegre");
        browser.findElement(By.id("field-state")).sendKeys("RS");
        browser.findElement(By.id("field-postalCode")).sendKeys("91000-000");
        browser.findElement(By.id("field-country")).sendKeys("Brasil");
        // browser.findElement(By.id("field-salesRepEmployeeNumber")).sendKeys("Fixter");
        browser.findElement(By.id("field-creditLimit")).sendKeys("200");

        WebElement btnSave = browser.findElement(By.id("form-button-save"));
        btnSave.click();

        // Validação da mensagem ao inserir registro com sucesso!
        String responseRegister = "Your data has been successfully stored into the database. Edit Record or Go back to list";

        assertEquals(responseRegister, browser.findElement(By.cssSelector("div#report-success p ")).getText());

        browser.close();

    }

}