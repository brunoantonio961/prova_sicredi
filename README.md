# prova_sicredi

Desafio técnico de automação Web, utilizando Java com Maven + Selenium WebDriver

# Informações técnicas

Código fora feito sobre a IDE Visual Code, utilizando como linguagem de programação Java, e importando e operacionalizando o teste com a biblioteca Selenium. Utilizado tambem, WebDriver para poder emular o navegador Google Chrome.

Utilizei inglês como linguagem para nomenclatura de alguns elementos e classe.

Não consegui realizar a parametrização dos dados, logo, acabaram permanecendo de uma forma estática.

# Passo realizado

Realizada a construção apenas do desafio 01, seguindo as orientações conforme reportado.

# Apontamentos

- Existem algumas divergências entre alguns Labels para massa de dados em detrimento ao disnponibilizado no site;
- O campo que deveria ser string com nomenclatura 'from Employeer' não existe no site, tendo apenas incidência de um campo denominado de "Sales Rep Employee Number" onde conforme nomenclatura, aceita apenas números, realizei algumas tentativas de incluir o texto "Fixter", porém, sem sucesso.

# Para executar

O próprio visual code possui uma extensão de "Testing", logo, o caso de teste pode ser executado diretamente pelo seu código, e será executado e reportado retorno através da ferramenta.

